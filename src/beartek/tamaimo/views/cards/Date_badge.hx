package beartek.tamaimo.views.cards;

import haxe.ui.macros.ComponentMacros;
import haxe.ui.core.Component;
import haxe.ui.components.Label;


class Date_badge extends View {

    public function new(date: Date) {
        super();

        var content: Label = this.component.findComponent('content');
        var widgets: Label = this.component.findComponent('widgets');

        content.text = DateTools.format(date, "%d/%m/%Y");       
    }

    override private function generate_component(): Component {
        var card = ComponentMacros.buildComponent('UI/cards/badge.xml');
        return card;
    }
}