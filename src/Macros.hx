
class Macros {
    macro static public function default_server(): haxe.macro.Expr.ExprOf<String> {
        return macro $v{if(haxe.macro.Context.defined("server_url")) haxe.macro.Context.definedValue("server_url") else 'https://matrix.org:8443'};
    }
}