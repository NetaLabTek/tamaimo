package beartek.tamaimo.views;

import haxe.ui.macros.ComponentMacros;
import haxe.ui.core.Component;
import haxe.ui.core.Screen;
import haxe.ui.components.TextField;
import haxe.ui.components.Button;
import haxe.ui.containers.dialogs.DialogButton;
import haxe.ui.containers.dialogs.DialogOptions;

class Login extends View {
    public function new() {
        super();

        var username_field: TextField = this.component.findComponent('username_place');
        var password_field: TextField = this.component.findComponent('password_place');
        var server_button: Button = this.component.findComponent('server-change');
        var submit_button: Button = this.component.findComponent('submit');

        server_button.onClick = function(e:Dynamic) {
            on_change_server_click();
        };

        submit_button.onClick = function(e:Dynamic) {
            #if sys
                var target_sys = Sys.systemName();
            #elseif js
                var target_sys = js.Browser.navigator.platform;
            #else
                var target_sys = 'unknow';
            #end

            Main.conn.session.login_with_pass(username_field.text, password_field.text, 'Tamaimo (' + CompileTime.buildGitCommitSha() + ') on ' + target_sys, function (resp: Dynamic) {
                trace(resp);
                on_login();
            });
        };

        if(Main.conn == null) Main.start_conn(Macros.default_server());
    }

    override private function generate_component(): Component {
        var box = ComponentMacros.buildComponent("UI/login/box.xml");
        return box;
    }

    public function on_login() {
        Main.log.log('info', 'Logged in: ' + Main.conn.session.user.toString());

        var dash = new Dash();
        this.remove_from_screen();
        dash.add_to_screen();
    }

    public function on_change_server_click() {
        var content = ComponentMacros.buildComponent("UI/dialogs/change_server.xml");
        var field : TextField = content.findComponent('server_url');
        field.text = if(Main.conn != null && Main.conn.server_url != null) Main.conn.server_url else Macros.default_server();

        var dialog = Screen.instance.showDialog(content, {icon: DialogOptions.ICON_INFO, title: 'Server address'}, function(btn: DialogButton) {
            if(btn.id == '1') {
                if(field.text != null) Main.start_conn(field.text);
            } else if(btn.closesDialog && Main.conn == null) {
                on_change_server_click();
            }
        });
    }
}