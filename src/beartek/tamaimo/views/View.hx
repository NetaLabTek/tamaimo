package beartek.tamaimo.views;

import haxe.ui.core.Component;

class View {
    public var component(default,null): Component;

    public function new() {
        this.component = generate_component();
    }

    private function generate_component(): Component {throw 'Override this';}

    public inline function add_to_screen() {
        Main.main_view.addComponent(this.component);
    }

    public inline function remove_from_screen() {
        Main.main_view.removeComponent(this.component);
        this.component.hide();
    }
}