package beartek.tamaimo.models;

import beartek.matrix_im.client.Conection;
import beartek.matrix_im.client.types.User;
import beartek.matrix_im.client.types.Event;
import beartek.matrix_im.client.types.replys.Joined_room;
import beartek.matrix_im.client.types.replys.Invited_room;
import beartek.matrix_im.client.types.replys.Left_room;
import beartek.tamaimo.models.User as User_model;

@:build(simple.bind.Macros.build())
class Room {
    public static var joined_rooms: Map<String, Room> = new Map();
    public static var invited_rooms: Map<String, Room> = new Map();
    public static var left_rooms: Map<String, Room> = new Map();
    static var onnew_triggers: Array<String->Room->Void> = [];
    static var onremove_triggers: Array<String->Room->Void> = [];


    public static function onsync_joined(rooms: Map<String, Joined_room>) {
        onsync(rooms, joined_rooms);
    }
    public static function onsync_invited(rooms: Map<String, Invited_room>) {
        onsync(rooms, invited_rooms);

    }
    public static function onsync_left(rooms: Map<String, Left_room>) {
        onsync(rooms, left_rooms);
    }

    public static function register_onnew(trigger: String->Room->Void) {
        onnew_triggers.push(trigger);
    }

    public static function register_onremove(trigger: String->Room->Void) {
        onremove_triggers.push(trigger);
    }

    private static function onsync(f_map: Map<String, Dynamic>, s_map: Map<String, Room>) {
        //TODO: better algorith, like concat the two array keys.
        for(room in f_map.keys()) {
            if (s_map[room] == null) {
                s_map[room] = new Room(new beartek.matrix_im.client.types.Room(room), true);
                for(trigger in onnew_triggers) {
                    trigger(room, s_map[room]);
                }
            }
        }

        //for(room in s_map.keys()) {
        //    if (f_map[room] == null) {
        //        s_map[room] = null;
        //        for(trigger in onremove_triggers) {
        //            trigger(room, s_map[room]);
        //        }
        //    }
        //}
    }

    public var id(default,null): beartek.matrix_im.client.types.Room;
    @bind public var name(get,set): String;
    @bind public var img: m.room.Avatar; 
    @bind public var description: String;
    @bind public var user_list: Map<String, {name: String, typing: Bool}> = new Map();
    @bind public var users_invited: Map<String, String> = new Map();
    @bind public var users_banned: Map<String, String> = new Map();
    @bind public var users_rank: Map<Int, Array<String>> = new Map();
    public var typings(default,null): Int; //TODO: Bind
    //TODO: last reads

    @bind public var events(default,default): {from: String, to: String, list: Array<Event<Dynamic>>} = {from: '', to: '', list: []}; //TODO: make readonly
    @bind public var pinned_events(default,default): Array<Event<Dynamic>> = []; //TODO: make readonly
    @bind public var notificication_events(default,default): {from: String, to: String, list: Array<Event<Dynamic>>} = {from: '', to: '', list: []}; //TODO: make readonly
    @bind public var unread(default,default): Int; //TODO: make readonly

    private var name_event: Event<m.room.Name>;
    private var syncing = false;

    function get_name(): String {
        var users: Array<String> = [];
        for(u in user_list.keys()) {
            users.push(if(user_list[u].name != null) user_list[u].name else u);
        }

        return this.id.get_name(this.name_event, users.iterator());
    }
    function set_name(name: String):String {
        //Main.conn.rooms.send_state_event(this.id, Name, {name: name}, function(id: String) {});

        return name;
    }

    //function set_description(d: String):String {
        //Main.conn.rooms.send_state_event(this.id, Topic, {topic: d}, function(id: String) {});

    //    return d;
    //}

    //function set_img(img: m.room.Avatar):m.room.Avatar {
        //Main.conn.rooms.send_state_event(this.id, Avatar, img, function(id: String) {});

    //    return img;
    //}

    public function new(id: beartek.matrix_im.client.types.Room, sync = false) {
        this.id = id;
        if(sync) {
            start_sync();
        }
    }

    private function get_pin(i = 0, list: Array<String>): Array<Event<Dynamic>> {
        if(i <= list.length-1) {
            //TODO: Room context
            //return result.concant(this.get_pin(i++, list));
            return [];
        } else {
            return [];
        }
    }

    @:access(beartek.tamaimo.models.User)
    public function start_sync() {
        if(syncing) throw 'Actually synching' else syncing = true;
        Main.conn.sync.add_room_handler(function(rooms: Map<String, Joined_room>) {
            for(id in rooms.keys()) {
                var id = new beartek.matrix_im.client.types.Room(id);
                if (id.equal(this.id)) {
                    for(e in rooms[id].state.events) {
                        switch e.type {
                        case Name:
                            var e: Event<m.room.Name> = e;
                            this.name_event = e;
                            simple.bind.Dispatcher.dispatch(this, 'name', null, this.get_name());
                        case Avatar:
                            var e: Event<m.room.Avatar> = e;
                            this.img = e.content;
                        case Topic:
                            var e: Event<m.room.Topic> = e;
                            this.description = e.content.topic;
                        case Power_levels:
                            var e: Event<m.room.Power_levels> = e;
                            var users: Map<String, Int> = Conection.to_object_map(e.content.users);

                            this.users_rank = new Map();
                            for(u in users.keys()) {
                                if(this.users_rank[users[u]] == null) this.users_rank[users[u]] = [];
                                this.users_rank[users[u]].push(u);
                            }
                        case Member:
                            var old_lenght = 0;
                            for(u in this.user_list.keys()) {
                                old_lenght++;
                                if(old_lenght > 3) break;
                            }

                            var e: Event<m.room.Member> = e;
                            if(e.content.membership == Join) this.user_list[e.sender] = {typing: false, name: e.content.displayname};
                            if(e.content.membership == Invite) this.users_invited[e.sender] = e.content.displayname;
                            if(e.content.membership == Ban) this.users_banned[e.sender] = e.content.displayname;

                            User_model.onadd(new User(e.sender));
                            User_model.users_list[e.sender].user.display_name = e.content.displayname;
                            User_model.users_list[e.sender].user.avatar_url = e.content.avatar_url;

                            if(old_lenght <= 3) simple.bind.Dispatcher.dispatch(this, 'name', null, this.get_name());
                        case Pinned_events:
                            var e: Event<m.room.Pinned_events> = e;
                            this.pinned_events = this.get_pin(e.content.pinned);
                        case _:
                            Main.log.log('Warning', 'Sync ' + this.id + ': Recived an event that is not implemented: ' + Std.string(e));
                        }
                    }

                    for(e in rooms[id].ephemeral.events) {
                        switch e.type {
                        case Typing:
                            var e: Event<m.Typing> = e;
                            this.typings = e.content.user_ids.length-1;
                            
                            for(u in this.user_list) {
                                u.typing = false;
                            }
                            for(user in e.content.user_ids) {
                                this.user_list[user.toString()].typing = true;
                            }
                        case Receipt:
                            Main.log.log('Warning', 'Sync ' + this.id + ': Recived receipt event that is not implemented: ' + Std.string(e));
                        case _:
                            Main.log.log('Error', 'Sync ' + this.id + ': Unexpected event: ' + Std.string(e));
                        }
                    }

                    this.unread = rooms[id].unread_notifications.notification_count;

                    this.events.from = rooms[id].timeline.prev_batch;
                    this.events.to = Main.conn.sync.batch['000'];
                    this.events.list = rooms[id].timeline.events;
                }
            }
        });
    }
}