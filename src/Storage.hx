import haxe.io.Bytes;
import mime.Mime.TypeInfo;
import com.hurlant.util.ByteArray;
import com.hurlant.crypto.symmetric.CBCMode;
import com.hurlant.crypto.symmetric.AESKey;
import com.hurlant.crypto.hash.SHA256;

class Storage {
    public var file_cache_index : Map<String, String> = new Map();
    public var decryptor : CBCMode;

    public function new(get_index: CBCMode->Map<String, String>, get_auth_file: Void->Bytes, save_auth_file: Bytes->Void, get_cache: String->Bytes, save_cache: String->Bytes->Void) {
        this.get_cache = get_cache;
        this.save_cache = save_cache;
        this.get_auth_file = get_auth_file;
        this.save_auth_file = save_auth_file;
        this.get_index = get_index;
    }

    public function onopen_handler(t:String) {
        var auth = this.get_auth();
        if(auth == null || auth.server != Main.conn.server_url || auth.auth_key != Main.conn.session.access_token) {
            this.set_auth(Main.conn.server_url, Main.conn.session.access_token);
        }
    }

    public function set_decryptor(key: String) {
        this.decryptor = new CBCMode(new AESKey(ByteArray.fromBytes(Bytes.ofString(key))));
        this.file_cache_index = get_index(this.decryptor);
    }

    public static function is_encrypted(): Bool {
        return false;
    }

    public function get_auth(): {server: String, auth_key: String} {
        if(!Storage.is_encrypted()) {
            var file = this.get_auth_file();
            if(file != null) {
                var data : {server: String, auth_key: String} = haxe.Json.parse(file.toString());
                return data;
            } else {
                return null;
            }
        } else {
            throw 'auth file encryption not implemented';
        }
    };

    public function set_auth(server: String, key:String) {
        if(!Storage.is_encrypted()) {
            this.save_auth_file(Bytes.ofString(haxe.Json.stringify({server: server, auth_key: key})));
        } else {
            throw 'auth file encryption not implemented';
        }
    }

    //public function get_cache(key:String): Dynamic {
    //   return null;
    //}

    //public function set_cache(key:String, value: Dynamic) {

    //}

    public function get_file_cache(id:String): {mime: TypeInfo, data: Bytes} {
        var data = ByteArray.fromBytes(this.get_cache(this.file_cache_index[id]));
        if(data.length == 0) {
            throw 'invalid cache file';
        } else {
            this.decryptor.decrypt(data);
            return haxe.Json.parse(data.getBytes().toString());
        }
    }

    public function set_file_cache(id:String, mime: TypeInfo, data: Bytes) {
        var hash_gen = new SHA256();

        var enc_data = ByteArray.fromBytes(Bytes.ofString(haxe.Json.stringify({mime: mime, data: data})));
        this.decryptor.encrypt(enc_data);

        var uuid = hash_gen.hash(enc_data);
        this.file_cache_index[id] = uuid.getBytes().toString();
        this.save_cache(uuid.getBytes().toString(), enc_data.getBytes());
    }


    private dynamic function get_index(dec: CBCMode): Map<String, String> {
        throw 'This fuction must be replaced';
    }

    private dynamic function get_auth_file(): Bytes {
        throw 'This fuction must be replaced';
    }

    private dynamic function save_auth_file(data: Bytes) {
        throw 'This fuction must be replaced';
    }

    private dynamic function get_cache(id: String): Bytes {
        throw 'This fuction must be replaced';
    }

    private dynamic function save_cache(id: String, data: Bytes) {
        throw 'This fuction must be replaced';
    }
}