package beartek.tamaimo.views.cards;

import haxe.ui.macros.ComponentMacros;
import haxe.ui.core.Component;
import haxe.ui.components.Label;
import haxe.ui.components.Image;
import beartek.matrix_im.client.types.Content_uri;
import beartek.tamaimo.models.User;

class User_list extends View {
    public var user(default, null): User;
    var name: Label;
    var status: Label;
    var img: Image;

    public function new(user: User) {
        super();

        this.user = user;

        simple.bind.Dispatcher.addListener(this.user, 'display_name', function(s:simple.bind.Dispatcher.FieldChangeSignal){
            this.name.text = s.to;
        });
        simple.bind.Dispatcher.addListener(this.user, 'avatar_url', function(s:simple.bind.Dispatcher.FieldChangeSignal){
            //URLs only works on js
            #if js
            this.img.resource = new Content_uri(s.to).toURL(Main.conn.server_url);
            #end
        });
        simple.bind.Dispatcher.addListener(this.user, 'active', function(s:simple.bind.Dispatcher.FieldChangeSignal){
            this.status.text = (if(s.to) 'Online ' else 'Offline ') + if(this.user.status != null) ' - ' + this.user.status else '';
        });
        simple.bind.Dispatcher.addListener(this.user, 'status', function(s:simple.bind.Dispatcher.FieldChangeSignal){
            this.status.text = (if(this.user.active) 'Online ' else 'Offline ') + ' - ' + s.to;
        });


        this.name.text = if(this.user.display_name != null || this.user.display_name != '') this.user.display_name else this.user.id.toString();
        this.status.text = (if(this.user.active) 'Online ' else 'Offline ') + if(this.user.status != null) ' - ' + this.user.status else '';
        //URLs only works on js
        #if js
        if(this.user.avatar_url != null) this.img.resource = new Content_uri(this.user.avatar_url).toURL(Main.conn.server_url);
        #end
    }

    override private function generate_component(): Component {
        var card = ComponentMacros.buildComponent('UI/cards/user_list.xml');

        this.name = card.findComponent('username');
        this.status = card.findComponent('status');
        this.img = card.findComponent('userimg');

        return card;
    }
}