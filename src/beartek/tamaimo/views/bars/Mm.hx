package beartek.tamaimo.views.bars;

import haxe.ui.macros.ComponentMacros;
import haxe.ui.core.Component;
import haxe.ui.components.Button;
import haxe.ui.components.Label;
import haxe.ui.components.Spacer;
import haxe.ui.containers.ScrollView;
import haxe.ui.containers.VBox;
import beartek.tamaimo.models.Room as Room_model;
import beartek.tamaimo.models.User;
import beartek.tamaimo.views.cards.User_list;
import beartek.tamaimo.views.cards.Message;

class Mm extends View {
    public var selected_room(default, set): Room_model;

    var members_tabs: Map<String, Button> = new Map();
    var messages_tabs: Map<String, Button> = new Map();
    var members_list: ScrollView;
    var messages_list: ScrollView;
    var members_type = Rank;
    var messages_type = Drafts;

    public function new() {
        super();

        for(id in members_tabs.keys()) {
            this.members_tabs[id].onClick = function(d: Dynamic) {
                switch id {
                    case 'rank':
                        this.members_type = M_list.Rank;
                        this.generate_members_list(this.selected_room);
                    case 'status':
                        this.members_type = M_list.Status;
                        this.generate_members_list(this.selected_room);
                    case 'others':
                        this.members_type = M_list.Other;
                        this.generate_members_list(this.selected_room);
                    case _:
                        Main.log.log('error', 'Unknow button clicked');
                }
            };
        }

        for(id in members_tabs.keys()) {
            this.members_tabs[id].onClick = function(d: Dynamic) {
                switch id {
                    case 'drafts':
                        this.messages_type = Msg_list.Drafts;
                        this.generate_msgs_list(this.selected_room);
                    case 'local-pins':
                        this.messages_type = Msg_list.Pins;
                        this.generate_msgs_list(this.selected_room);
                    case 'notifs':
                        this.messages_type = Msg_list.Notifications;
                        this.generate_msgs_list(this.selected_room);
                    case _:
                        Main.log.log('error', 'Unknow button clicked');
                }
            };
        }
    }

    public function set_selected_room(r: Room_model): Room_model {
        this.generate_members_list(r);

        this.selected_room = r;
        return r;
    }

    @:access(haxe.ui.containers.ListView._contents) //Workaround for addComponent
    public inline function generate_members_list(room: Room_model) {
        this.members_list.clearContents();

        switch this.members_type {
            case Rank:
                for(rank in room.users_rank.keys()) {
                    this.members_list.addComponent(this.generate_divider(Std.string(rank)));

                    for(member in room.users_rank[rank]) {
                        try {
                            if(room.user_list[member] != null) this.members_list.addComponent(new User_list(User.users_list[member].user).component);
                        } catch(e:String) {
                            Main.log.log('error', e);
                        }
                    }
                }
            case Status:
                //TODO: It can be tedius loading all users and divide online and offline users
                for(u in room.user_list.keys()) {
                    this.members_list.addComponent(new User_list(User.users_list[u].user).component);
                }
            case Other:
                this.members_list.addComponent(this.generate_divider('Invited'));
                for(member in room.users_invited.keys()) {
                    this.members_list.addComponent(new User_list(User.users_list[member].user).component);
                }

                this.members_list.addComponent(this.generate_divider('Banned'));
                for(member in room.users_banned.keys()) {
                    this.members_list.addComponent(new User_list(User.users_list[member].user).component);
                }
        }

    }

    public inline function generate_msgs_list(room: Room_model) {
            this.members_list.clearContents();

            switch this.messages_type {
            case Drafts:
                this.messages_list.onScrollChange = null;

                //TODO: Storage
            case Pins:
                this.messages_list.onScrollChange = null;

                for(e in this.selected_room.pinned_events) {
                    if(e.type == Message) {
                        this.messages_list.addComponent(new Message(e).component);
                    }
                }
            case Notifications:
                this.messages_list.onScrollChange = function(d:Dynamic) {
                    if(this.messages_list.vscrollPos-3 < 0) {
                        //TODO: get more
                    }
                }

                for(e in this.selected_room.notificication_events.list) {
                    if(e.type == Message) {
                        this.messages_list.addComponent(new Message(e).component);
                    }
                }
                this.messages_list.vscrollPos = 0;
        }
    }

    public function generate_divider(text: String): VBox {
        var result = new VBox();
        var label = new Label();
        var div = new Spacer();

        result.percentWidth = 100;
        label.text = text;
        label.addClass('fixed-divider-text');
        div.percentWidth = 100;
        div.addClass('divider');

        result.addComponent(div);
        result.addComponent(label);
        return result;
    }

    override private function generate_component(): Component {
        var box = ComponentMacros.buildComponent('UI/main/mm-bar.xml');

        this.members_tabs = ['rank' => box.findComponent('rank'), 'status' => box.findComponent('status'), 'others' => box.findComponent('other')];
        this.messages_tabs = ['drafts' => box.findComponent('drafts'), 'local-pins' => box.findComponent('local-pins'), 'notifs' => box.findComponent('notifs')];
        this.members_list = box.findComponent('members-tab');
        this.messages_list = box.findComponent('messages-tab');

        return box;
    }
}

enum M_list {
    Rank;
    Status;
    Other;
}

enum Msg_list {
    Drafts;
    Pins;
    Notifications;
}