package beartek.tamaimo.views.bars;

import haxe.ui.macros.ComponentMacros;
import haxe.ui.core.Component;
import haxe.ui.components.Image;
import haxe.ui.containers.VBox;
import haxe.ui.containers.ScrollView;
import beartek.matrix_im.client.types.replys.Joined_room;
import beartek.matrix_im.client.types.Content_uri;
//import beartek.matrix_im.client.types.Room;
import beartek.tamaimo.views.cards.Room as Room_card;
import beartek.tamaimo.models.Room as Room_model;


class Menu extends View {
    var room_cards: Map<String, Room_card> = new Map();
    var on_room_click: String->Void;

    var me_img: Image;
    var groups: ScrollView;

    public function new(on_room_click: String->Void) {
        super();
        this.on_room_click = on_room_click;

        this.start_me_menu();

        Room_model.register_onnew(function(id: String, model: Room_model) {
            this.add_card(id, model);
        });
        Room_model.register_onremove(function(id: String, model: Room_model) {
            var bar : VBox = cast(this.component);

            room_cards[id] = null;
            bar.removeComponent(room_cards[id].component);
        });

        for(room in Room_model.joined_rooms.keys()) {
            this.add_card(room, Room_model.joined_rooms[room]);
        }
    }

    private inline function add_card(id: String, model: Room_model) {
        room_cards[id] = new Room_card(model);
        room_cards[id].component.onClick = function(e: Dynamic) {
            this.on_room_click(id);
        };
        this.groups.addComponent(room_cards[id].component);
    }

    private inline function start_me_menu() {
        Main.conn.profile.get_avatar(Main.conn.session.user,function(url:String) {
            //URLs only works on js
            #if js
            this.me_img.resource = new Content_uri(url).toURL(Main.conn.server_url);
            #end
        });
    }

    override private function generate_component(): Component {
        var box = ComponentMacros.buildComponent('UI/main/menu-bar.xml');

        this.me_img = box.findComponent('me-img');
        this.groups = box.findComponent('groups');

        return box;
    }
}