# Tamaimoe
***Warning: this application is in a prealpha state, so only about 20% are finished and there a lot of things to polish***.

![Tamaimo](assets/presentation/main-html5.jpg)

Tamaimoe is an instant messaging application that uses the matrix protocol (in the future others, ex: **xmpp**), therefore is federated (decentralized) and unified with other platforms, it is also multiplatform, having a web version *with a web server (soon) to offer files and invitation links*. 

On the other hand it offers quite complete chat functionalities, besides providing extra functionalities to be able to create communities and blogs, having blog mode and forum mode. It also has a focus on customization, allowing themes, plugins, widgets, bots, integration with other platforms and customization to communities without breaking the uniform aesthetics of the app. Giving it great flexibility for various types of uses. It also has a wide configuration arranged to facilitate the tasks of administration and to be simple.

It provides an interface that seeks ergonomics, without redundancy and minimalism.

It is also perfect for meetings, having its own voice chat that will not depend on webrtc technology, being able to integrate codecs designed for voice. In addition to having a customizable voice interface, adaptable and integrable (overlay in videogames).

Since the technical part is made in haxe and haxeui, is adaptable to many platforms and graphics backends, the application can be used with the custom interface, native system or terminal. In addition, it can be transpiled to various languages *js, c, c++, neko, java(?)* and various devices such as PCs, mobile phones, consoles, ...

## Installation
It is not yet complete enough to be installed.

## Compilation 
The compilation process can be a bit tedious because both this app and the haxeUI framework are incomplete. 
First, you will have to install **haxe and haxelib**, we recommend version *3.4*.

The second thing will be to install the following libraries of haxelib:
``` 
haxe-crypto
matrix-im
simplebinds
easylog
mime
compiletime
hext-sort
hext-search
markdown
``` 
Also you will have to install from git, for the moment, the forks of [akifox-http](https://github.com/endes123321/akifox-asynchttp) and [haxeui-core](https://github.com/endes123321/haxeui-core).

Finally you will have to compile with haxe, you can install openfl and use ```openfl run``` or compile the html version with ```haxe build.hxml``` or the native version with ```haxe build-wxwidgets.hxml``` or the terminal version with ```haxe build-curse.hxml```.

## Develop
Not yet complete enough to develop plugins, themes, etc ...

### Themes

### Plugins

### bots, integrations, ...
This is done through the matrix protocol, in [its page you can find more information.](https://matrix.org/docs/projects/try-matrix-now.html#client-sdks)

## Contribute
I have to organize a lot of things for this point, just open an issue or pull request and we'll talk.

## Credits
TODO.