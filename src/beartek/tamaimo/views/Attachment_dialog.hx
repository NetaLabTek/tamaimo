package beartek.tamaimo.views;

import haxe.ui.macros.ComponentMacros;
import haxe.ui.core.Component;
import haxe.ui.components.Image;
import haxe.ui.components.Button;
import beartek.matrix_im.client.types.Content_uri;
import beartek.matrix_im.client.types.enums.Msg_types;

class Attachment_dialog extends View {
    var close: Image;
    var file_btns: Map<String, Button> = new Map();

    var send_msg: Dynamic -> Void;

    public function new(send_msg: Dynamic -> Void) {
        super();
        this.send_msg = send_msg;

        close.onClick = function(d:Dynamic) {
            this.remove();
        };
        for(b in this.file_btns.keys()) {
            this.file_btns[b].onClick = function(d:Dynamic) {
                #if js
                    var d = js.Browser.document.createInputElement();
                    d.type="file";
                    d.click();

                    d.onchange = function(_) {
                        for(file in d.files) {
                            var binary = file;
                            if(file.type.split('/')[0] == 'image' && b == 'image') {
                                Main.conn.content.upload(file.name, file.type, binary, function(uri: Content_uri) {
                                    this.send_image(uri, file.name);
                                });
                            } else if(file.type.split('/')[0] == 'video' && b == 'video') {
                                Main.conn.content.upload(file.name, file.type, binary, function(uri: Content_uri) {
                                    this.send_video(uri, file.name);
                                });
                            } else if(file.type.split('/')[0] == 'audio' && b == 'audio') {
                                Main.conn.content.upload(file.name, file.type, binary, function(uri: Content_uri) {
                                    this.send_audio(uri, file.name);
                                });
                            } else if(file.type.split('/')[0] != 'image' && file.type.split('/')[0] != 'video' && file.type.split('/')[0] != 'audio' && b == 'file') {
                                Main.conn.content.upload(file.name, file.type, binary, function(uri: Content_uri) {
                                    this.send_file(uri, file.name);
                                });
                            } else {
                                js.Browser.alert('Incorret or unknow file type');
                            }
                        }
                    d.remove();
                    }
                #else
                
                #end
            }
        }
    }

    public inline function send_image(uri: Content_uri, body: String) {
        var msg = {msgtype: Msg_types.Image, body: body, url: uri.toString()};
        this.send_msg(msg);
    }

    public inline function send_video(uri: Content_uri, body: String) {
        var msg = {msgtype: Msg_types.Video, body: body, url: uri.toString()};
        this.send_msg(msg);
    }

    public inline function send_audio(uri: Content_uri, body: String) {
        var msg = {msgtype: Msg_types.Audio, body: body, url: uri.toString()};
        this.send_msg(msg);
    }

    public inline function send_file(uri: Content_uri, body: String) {
        var msg = {msgtype: Msg_types.File, body: body, url: uri.toString()};
        this.send_msg(msg);
    }

    public inline function remove() {
        this.component.parentComponent.findComponent('send-box').show();
        this.component.parentComponent.removeComponent(this.component);
    }

    override private function generate_component(): Component {
        var box = ComponentMacros.buildComponent('UI/dialogs/send_attachment.xml');

        this.close = box.findComponent('close');
        this.file_btns = ['image' => box.findComponent('img'), 'video' => box.findComponent('video'), 'file' => box.findComponent('file'), 'audio' => box.findComponent('audio')];

        return box;
    }
}