package beartek.tamaimo.views.cards;

import haxe.ui.core.Component;
import haxe.ui.macros.ComponentMacros;
import haxe.ui.components.Label;
import haxe.ui.components.Image;
//import beartek.matrix_im.client.types.Room as R;
import beartek.matrix_im.client.types.Content_uri;
import beartek.tamaimo.models.Room as Room_model;

class Room extends View {
    public var room(default, null): Room_model;

    var img: Image;

    public function new(room: Room_model) {
        super();

        this.room = room;

        simple.bind.Dispatcher.addListener(this.room, 'name', function(s:simple.bind.Dispatcher.FieldChangeSignal){
            var name: Label = this.component.findComponent('room-name');
            name.text = s.to;
        });
        simple.bind.Dispatcher.addListener(this.room, 'unread', function(s:simple.bind.Dispatcher.FieldChangeSignal){
            var notif: Label = this.component.findComponent('room-notifications');
            notif.text = '' + s.to;
            if(Std.parseInt(notif.text) > 0) notif.hidden = false else notif.hidden = true;
        });
        simple.bind.Dispatcher.addListener(this.room, 'img', function(s:simple.bind.Dispatcher.FieldChangeSignal){
            //URLs only works on js
            #if js
            this.img.resource = new Content_uri(this.room.img.url).toURL(Main.conn.server_url);
            check_img_size();
            #end
        });

        var name: Label = this.component.findComponent('room-name');
        name.text = this.room.name;
        var notif: Label = this.component.findComponent('room-notifications');
        notif.text = '' + this.room.unread;
        if(this.room.unread > 0) notif.hidden = false else notif.hidden = true;
        //URLs only works on js
        #if js
        if(this.room.img != null && this.room.img.url != null) this.img.resource = new Content_uri(this.room.img.url).toURL(Main.conn.server_url);
        check_img_size();
        #end
    }

    private function check_img_size() {
        if(true) {  //(this.img.width > this.component.width || this.img.height > this.component.height) {
            this.img.width = 130;//this.component.width;
            this.img.height = 130;//this.component.width;
        }
    }

    override private function generate_component(): Component {
        var card = ComponentMacros.buildComponent('UI/cards/room.xml');

        this.img = card.findComponent('room-img');

        return card;
    }
}