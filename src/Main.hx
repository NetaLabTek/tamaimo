import beartek.tamaimo.models.Room as Room_model;
import beartek.tamaimo.views.Login;
import beartek.matrix_im.client.Conection;

import easylog.EasyLogger;
import haxe.ui.HaxeUIApp;
import haxe.ui.Toolkit;
import haxe.ui.macros.ComponentMacros;
import haxe.ui.core.Screen;
import haxe.ui.core.Component;
import  haxe.ui.containers.Box;
import haxe.ui.containers.dialogs.DialogOptions;
import haxe.io.Bytes;

class Main {
    static public var conn: Conection;
    static public var storage: Storage;
    static public var log = new EasyLogger("tamaimo_[logType].log");
    static public var main_view: Component;
    #if (!js || nodejs)
    static public var config_dirs: {logs: String, auth_data: String, cache_data: String} = CompileTime.parseJsonFile('dir_config.json');
    #end

    static function main() {
        log.log('info', 'Starting Tamaimo...');
        var app = new HaxeUIApp();
        app.ready(function() {
            if(Storage.is_encrypted()) {
                start_decrypt_ui();
            } else {
                start_sequence();
            }

            app.start();
        });
    }

    public static inline function start_sequence() {
        start_storage();
        start_conn();
        //start_ui();
    }

    public static function start_storage() {
        #if sys
            if(!sys.FileSystem.exists(Main.config_dirs.cache_data)) sys.FileSystem.createDirectory(Main.config_dirs.cache_data);
            if(!sys.FileSystem.exists(Main.config_dirs.auth_data)) sys.FileSystem.createDirectory(Main.config_dirs.auth_data);
            if(!sys.FileSystem.exists(Main.config_dirs.logs)) sys.FileSystem.createDirectory(Main.config_dirs.logs);

            if(!sys.FileSystem.exists(Main.config_dirs.cache_data + '/index.ejson')) sys.io.File.saveBytes(Main.config_dirs.cache_data + '/index.ejson', Bytes.ofString(''));
            if(!sys.FileSystem.exists(Main.config_dirs.auth_data + '/auth')) sys.io.File.saveBytes(Main.config_dirs.auth_data + '/auth', Bytes.ofString(''));
        #end

        Main.storage = new Storage(function (dec:com.hurlant.crypto.symmetric.CBCMode): Map<String, String> {
            var index: Map<String, String>;

            #if sys
                var data = com.hurlant.util.ByteArray.fromBytes(sys.io.File.getBytes(Main.config_dirs.cache_data + '/index.ejson'));
            #elseif nodejs
                var data = com.hurlant.util.ByteArray.fromBytes(js.node.Fs.readFileSync(Main.config_dirs.cache_data + '/index.ejson').hxToBytes());
            #elseif js
                var data = com.hurlant.util.ByteArray.fromBytes(Bytes.ofString(js.Browser.window.localStorage.getItem('cache_index.ejson')));
            #end

        if(data.length == 0) {
            index = new Map();
            return index;
        } else {
            dec.decrypt(data);
            index = haxe.Json.parse(data.getBytes().toString());
            if(Std.is(index, Map)) {
                for (file in index.keys()) {
                    #if sys
                        var exist = sys.FileSystem.exists(Main.config_dirs.cache_data + '/' + file + '.ecache');
                    #elseif nodejs
                        var exist = true;
                        try {
                            var data = js.node.Fs.accessSync(Main.config_dirs.cache_data + '/' + file + '.ecache').hxToBytes();
                        } catch(msg: js.Error) {
                            //TODO: log
                            exist = false;
                        }
                    #elseif js
                        var exist = if(js.Browser.window.localStorage.getItem('cache_' + file + '.ecache') == null) false else true;
                    #end

                    if(!exist) {
                        index.remove(file);
                    }
                }

                return index;
            } else {
                throw "Bad cache index";
            }
        }
        }, function (): Bytes {
            #if sys
                var data = sys.io.File.getBytes(Main.config_dirs.auth_data + '/auth');
                if(data.toString() == '') data = null;
            #elseif nodejs
                var data = js.node.Fs.readFileSync(Main.config_dirs.auth_data + '/auth').hxToBytes();
            #elseif js
                var file = js.Browser.window.localStorage.getItem('auth_data');
                var data: Bytes = null;
                if(file != null) {
                    data = Bytes.ofString(file);
                } else {
                    Main.log.log('Warning', 'No auth data exist');
                }
            #end

            return data;
        }, function (file: Bytes) {
            #if sys
                sys.io.File.saveBytes(Main.config_dirs.auth_data + '/auth', file);
            #elseif nodejs
                js.node.Fs.writeFileSync(Main.config_dirs.auth_data + '/auth', js.node.buffer.Buffer.hxFromBytes(file));
            #elseif js
                js.Browser.window.localStorage.setItem('auth_data', file.toString());
            #end
        }, function (file: String): Bytes {
            #if sys
                var data = sys.io.File.getBytes(Main.config_dirs.cache_data + '/' + file);
            #elseif nodejs
                var data = js.node.Fs.readFileSync(Main.config_dirs.auth_data + '/auth').hxToBytes();
            #elseif js
                var data = Bytes.ofString(js.Browser.window.localStorage.getItem('cache_' + file));
            #end

            return data;
        }, function (file: String, data: Bytes) {
            #if sys
                sys.io.File.saveBytes(Main.config_dirs.cache_data + '/' + file, data);
            #elseif nodejs
                js.node.Fs.writeFileSync(Main.config_dirs.cache_data + '/' + file, js.node.buffer.Buffer.hxFromBytes(data));
            #elseif js
                js.Browser.window.localStorage.setItem('cache_' + file, data.toString());
            #end
        });
    }

    public static function start_conn(?server: String) {
        var auth = Main.storage.get_auth();

        if(auth != null) {
            Main.conn = new Conection(auth.server);
            register_handlers();
            Main.conn.session.onopen(function(s:String) {
                start_ui();
            });
            Main.conn.session.access_token = auth.auth_key;
        } else if(server != null) {
            Main.conn = new Conection(server);
            register_handlers();
            start_ui();
        } else {
            Main.conn = null;
            start_ui();
        }
    }

    private static function register_handlers() {
        Main.conn.session.onopen(Main.storage.onopen_handler);
        Main.conn.session.onopen(function(s:String) {
            Main.sync();
        });
        Main.conn.sync.add_room_handler(Room_model.onsync_joined, Room_model.onsync_invited, Room_model.onsync_left);
    }

    private static function sync() {
        Main.conn.sync.add_account_data_handler(function(e:Array<Dynamic>) {
            Main.conn.sync.sync(null, null,15000);
        });
        Main.conn.sync.sync(null, null,15000);
    }

    public static function start_ui() {
        try {
            Toolkit.pixelsPerRem = 21;
            Toolkit.styleSheet.addRules(CompileTime.readFile('UI/theme/old.css'));
            Toolkit.styleSheet.addRules(CompileTime.readFile('UI/theme/spectre/dist/spectre.css'));
        } catch(e:String) {
            trace(e);
        }
        
        Main.main_view = new Box();
        Main.main_view.percentWidth = 100;
        Main.main_view.percentHeight = 100;

        var login = new Login();
        if(Main.conn != null && Main.conn.session.access_token != null) {
            login.on_login();
        } else {
            login.add_to_screen();
        }

        Screen.instance.addComponent(Main.main_view);
    }

    public static function start_decrypt_ui() {
        var decrypt = ComponentMacros.buildComponent('UI/dialogs/decrypt.xml');
        var dialog = Screen.instance.showDialog(decrypt);

        dialog.dialogOptions.icon = DialogOptions.ICON_INFO;
        dialog.dialogOptions.title = 'PIN';
        dialog.dialogOptions.buttons[0].closesDialog = false;
    }

    public static inline function bad_pin() {
        Screen.instance.messageDialog('Incorrect PIN.', 'Tamaimo: incorrect PIN', {icon: DialogOptions.ICON_ERROR},
                    function(btn: Dynamic) {
                        #if js
                            js.Browser.location.reload();
                        #end
                        #if sys
                            Sys.exit(0); //TODO: Code
                        #end
        });
    }
}
