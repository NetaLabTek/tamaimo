package beartek.tamaimo.views.cards;

import haxe.ui.macros.ComponentMacros;
import haxe.ui.core.Component;
import haxe.ui.components.Label;
import haxe.ui.containers.ScrollView;
import beartek.matrix_im.client.types.Event;
import beartek.matrix_im.client.types.enums.Msg_types;


class Badge extends View {
    var msg: Event<Dynamic>;

    public function new(msg: Event<Dynamic>) {
        super();

        this.msg = msg;

        var content: Label = this.component.findComponent('content');
        var widgets: Label = this.component.findComponent('widgets');

        content.text = this.generate_content();
    }

    public function generate_content(): String {
        if(Reflect.field(msg.content, 'body') != null && Reflect.field(msg.content, 'msgtype') == Emote) {
            return '<p><b>' + msg.sender + '</b> ' + msg.content.body + '</p>'; //TODO: displayname and non html.
        } else if(Reflect.field(msg.content, 'membership') != null) {
            return '<p><b>' + msg.sender + '</b> has ' + msg.content.membership + '</p>';
        } else if(Reflect.field(msg.content, 'users_default') != null) {
            return '<p><b>' + msg.sender + '</b> changed power levels</p>';
        } else if(Reflect.field(msg.content, 'creator') != null) {
            return '<p><b>' + msg.sender + '</b> created the room</p>';
        } else if(Reflect.field(msg.content, 'join_rule') != null) {
            return '<p><b>' + msg.sender + '</b> changed the join rule</p>';
        } else if(Reflect.field(msg.content, 'history_visibility') != null) {
            return '<p><b>' + msg.sender + '</b> changed the history visibility to ' + msg.content.history_visibility + '</p>';
        } else if(Reflect.field(msg.content, 'guest_access') != null) {
            return '<p><b>' + msg.sender + '</b> ' + msg.content.guest_access + ' guest access to the room</p>';
        } else {
            Main.log.log('error', 'not implemented');
            trace(msg.content);
            return 'Unknow action';
        }
    }

    override private function generate_component(): Component {
        var card = ComponentMacros.buildComponent('UI/cards/badge.xml');
        return card;
    }
}