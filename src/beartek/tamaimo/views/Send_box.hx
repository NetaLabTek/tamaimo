package beartek.tamaimo.views;

import haxe.ui.macros.ComponentMacros;
import haxe.ui.core.Component;
import haxe.ui.core.KeyboardEvent;
import haxe.ui.core.UIEvent;
import haxe.ui.components.Image;
import haxe.ui.components.TextArea;
import haxe.ui.components.Switch;
import haxe.ui.components.Button;
import beartek.matrix_im.client.types.enums.Msg_types;
import beartek.tamaimo.models.Room as Room_model;


class Send_box extends View {
    public var selected_room: Room_model;
    var send: Button;
    var commands: Button;
    var attachment: Button;
    var area: TextArea;
    var md_switch: Switch;
    var format_btns: Map<String, Button> = new Map();

    var short_draft = false;
    var long_draft = false;
    var msg_sent = false;
    var add_self_msg: m.room.Message -> Void;

    public function new(add_self_msg: m.room.Message -> Void) {
        super();
        this.add_self_msg = add_self_msg;

        send.onClick = function(d:Dynamic) {
            this.send_text_msg();
        };
        area.registerEvent(KeyboardEvent.KEY_DOWN, function(e:KeyboardEvent) {
            if(e.keyCode == 13) {
            this.send_text_msg();
            }
        });
        area.onChange = function(d:Dynamic) {
            if(!short_draft) haxe.Timer.delay(function() {
                if(!this.area.focus && !this.area.empty) {
                    //TODO: Storage save draft
                    short_draft = false;
                }
            }, 5500);
            if(!short_draft) short_draft = true;

            if(!long_draft) {
                haxe.Timer.delay(function() {
                    if(!this.area.empty && !msg_sent) {
                        //TODO: Storage save draft
                        long_draft = false;
                        msg_sent = false;
                    }
                }, 30000);
                long_draft = true;
                msg_sent = false;
            }
        };
        md_switch.onClick = function(d:Dynamic) {
            if(md_switch.selected) {
                for(b in this.format_btns) {
                    b.disabled = false;
                }
            } else {
                for(b in this.format_btns) {
                    b.disabled = true;
                }
            }
        };
        attachment.onClick = function(d:Dynamic) {
            this.component.hide();
            this.component.parentComponent.addComponentAt(new Attachment_dialog(this.send_msg).component, -1);
        };
        for(elem in this.format_btns.keys()) {
            this.format_btns[elem].onClick = function(d:Dynamic) {
                switch elem {
                    case 'bold':
                        this.area.text += '**';
                    case 'italic':
                        this.area.text += '*';
                    case 'underline':
                        this.area.text += '__';
                    case _:
                        Main.log.log('error', 'Unknow format button');
                }
            };
        }
    }

    private inline function send_text_msg() {
        //TODO: remove markdown syntax from body
        var msg = {msgtype: Msg_types.Text, body: this.area.text};
        var format_msg = {msgtype: Msg_types.Text, body: this.area.text, formatted_body: Markdown.markdownToHtml(this.area.text), format: 'org.matrix.custom.html'};
            if(this.selected_room != null && !this.area.empty) {
                msg_sent = true;
                this.send_msg(if(this.md_switch.selected) format_msg else msg);
                this.area.text = '';
            }
    }

    private function send_msg(msg: Dynamic) {
        Main.conn.rooms.send_event(this.selected_room.id, Message, null, msg, function(id:String) {
            this.add_self_msg(msg);
            this.selected_room.events.list.push({event_id: '', sender: Main.conn.session.user.toString(), content: msg, type: Message, origin_server_ts: Std.int(Date.now().getTime())});
        }); 
    }

    override private function generate_component(): Component {
        var box = ComponentMacros.buildComponent('UI/main/send-box.xml');

        this.send = box.findComponent('send-icon');
        this.commands = box.findComponent('commands-icon');
        this.attachment = box.findComponent('send-attachment-icon');
        this.area = box.findComponent('area');
        this.md_switch = box.findComponent('md');
        this.format_btns = ["bold" => box.findComponent('b'), "italic" => box.findComponent('i'), "underline" => box.findComponent('u')];

        return box;
    }
}