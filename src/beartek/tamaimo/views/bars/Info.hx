package beartek.tamaimo.views.bars;

import haxe.ui.core.Component;
import haxe.ui.core.Screen;
import haxe.ui.components.Image;
import haxe.ui.components.Label;
import haxe.ui.macros.ComponentMacros;
import beartek.tamaimo.models.Room as Room_model;
import beartek.matrix_im.client.types.Content_uri;

class Info extends View {
    public var selected_room(default, set): Room_model;

    var img:Image;
    var title:Label;
    var description:Label;

    public function new() {
        super();

        this.component.onClick = function(d:Dynamic) {
            if(this.selected_room != null) {
                var diag = Screen.instance.showDialog(this.create_info_dialog(), {title: 'Info', buttons: []});
            }
        }
    }

    public inline function create_info_dialog(): Component {
        var box = ComponentMacros.buildComponent('UI/dialogs/info.xml');
        var info_img:Image = box.findComponent('img');
        var info_title:Label = box.findComponent('title');
        var info_description:Label = box.findComponent('description');

        if(this.selected_room.img != null) info_img.resource = new Content_uri(this.selected_room.img.url).toURL(Main.conn.server_url);
        info_title.text = this.selected_room.name;
        info_description.text = this.selected_room.description;
        //TODO: dynamic change

        return box;
    }

    public function set_selected_room(r: Room_model): Room_model {
        if(r.img != null) this.img.resource = new Content_uri(r.img.url).toURL(Main.conn.server_url);
        this.title.text = r.name;
        this.description.text = r.description;
        //TODO: dynamic change
        this.selected_room = r;
        return r;
    }

    override private function generate_component(): Component {
        var box = ComponentMacros.buildComponent('UI/main/info-bar.xml');

        this.img = box.findComponent('img');
        this.title = box.findComponent('title');
        this.description = box.findComponent('description');

        return box;
    }
}