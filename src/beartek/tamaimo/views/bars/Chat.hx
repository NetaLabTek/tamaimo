package beartek.tamaimo.views.bars;

import haxe.ui.core.Component;
import haxe.ui.layouts.HorizontalLayout;
import haxe.ui.containers.VBox;
import haxe.ui.containers.ScrollView;
import beartek.matrix_im.client.types.Event;
import beartek.matrix_im.client.types.enums.Directions;
import beartek.tamaimo.views.cards.Message;
import beartek.tamaimo.views.cards.Badge;
import beartek.tamaimo.views.cards.Date_badge;
import beartek.tamaimo.models.Room as Room_model;
using StringTools;

class Chat extends View {
    public var selected_room(default, set): Room_model;

    var msgs_box: ScrollView;
    var info_bar: Info;
    var send_box: Send_box;
    var batchs: Array<String> = [];
    var batchs_time_up: Map<String, Int> = new Map();
    var batchs_time_down: Map<String, Int> = new Map();
    var next: String;
    var min_msg_reached: Bool = false;
    var max_reached: Bool = false;
    var min_reached: Bool = false;

    @:access(haxe.ui.containers.ScrollView._contents) //Workaround for onChange
    public function new() {
        super();

        this.send_box = new Send_box(function(msg: m.room.Message) {
            if(this.selected_room.events.from == this.batchs[this.batchs.length-1]) {
                var comp = new Message({event_id: '', sender: Main.conn.session.user.toString(), content: msg, type: Message, origin_server_ts: Std.int(Date.now().getTime())});
                this.msgs_box.addComponent(comp.component);
            }
        });
        this.msgs_box.onScrollChange = function(e:Dynamic) {
            if(this.msgs_box.vscrollPos < 3 && min_msg_reached == false && min_reached == false) {
                //TODO: Storage
                min_reached = true;
                Main.conn.rooms.get_messages(this.selected_room.id, this.batchs[0], Directions.Backward, 10, null, function(s: String, end: String, e: Array<Event<Dynamic>>) {
                    e.reverse();
                    this.generate_timeline(end, s, e);
                    min_reached = false;
                });
            } else if(this.msgs_box.vscrollPos+5 >= this.msgs_box.vscrollMax && this.batchs[this.batchs.length-1] != this.selected_room.events.from  && max_reached == false) {
                if(next == null) {

                } else {
                    max_reached = true;
                    Main.conn.rooms.get_messages(this.selected_room.id, next, Directions.Forward, 10, null, function(s: String, end: String, e: Array<Event<Dynamic>>) {
                        this.generate_timeline(s, end, e);
                        max_reached = false;
                    });
                }
            }
        }

        this.msgs_box.registerEvent(haxe.ui.core.UIEvent.RESIZE, function(t:Dynamic) {
            if(this.msgs_box._contents != null && this.msgs_box._contents.height < this.msgs_box.height+5 && min_msg_reached == false && this.selected_room != null) Main.conn.rooms.get_messages(this.selected_room.id, this.batchs[0], Directions.Backward, 10, null, function(s: String, end: String, e: Array<Event<Dynamic>>) {
                    e.reverse();
                    this.generate_timeline(end, s, e);
                    min_reached = false;
                });
        });

        this.component.addComponent(this.send_box.component);
    }

    public function set_selected_room(r: Room_model): Room_model {
        this.msgs_box.clearContents(); //TODO: report, if you put removeAllComponents, its crash
        this.batchs = [];
        this.next = null;
        this.min_msg_reached = false;
        this.generate_timeline(r.events.from, r.events.list);

        this.selected_room = r;
        this.send_box.selected_room = r;
        this.info_bar.selected_room = r;
        this.msgs_box.dispatch(new haxe.ui.core.UIEvent(haxe.ui.core.UIEvent.RESIZE));
        return r;
    }

    @:access(haxe.ui.containers.ScrollView._contents) //Workaround for addComponentAt, findComponent and getComponentIndex
    public function generate_timeline(batch: String, ?next_batch: String, events: Array<Event<Dynamic>>) {
        var searcher = new hext.search.array.BinarySearch(this.compare_batchs);
        if(searcher.indexOf(batch, this.batchs) != -1) return;
        if(next_batch != null && searcher.indexOf(next_batch, this.batchs) == -1) {
            this.msgs_box.clearContents();
            this.batchs = [];
        }
        this.next = next_batch;

        batchs.push(batch);
        if(batchs.length > 1) hext.sort.algorithms.QuickSort.sort(this.batchs, this.compare_batchs);
        var index = searcher.indexOf(batch, this.batchs);

        var last_day: Null<Int> = null;

        if(this.batchs.length < index+2) {
            this.generate_indicator(batch);
            for(msg in events) {
                var date_badge = this.check_day(msg.origin_server_ts, last_day, index);
                if(date_badge != null) {
                    this.msgs_box.addComponent(date_badge);
                    last_day = msg.origin_server_ts;
                }; 

                var comp: Component;
                if(Reflect.field(msg.content, 'body') != null || Reflect.field(msg.content, 'msgtype') != null) {
                    comp = new Message(msg).component;
                } else {
                    comp = new Badge(msg).component;
                    if(Reflect.field(msg.content, 'creator') != null) {
                        this.min_msg_reached = true;
                    }
                }
                this.msgs_box.addComponent(comp);
            }
            this.batchs_time_down[this.batchs[index]] = events.pop().origin_server_ts;
        } else {
            var start = this.msgs_box._contents.getComponentIndex(this.msgs_box._contents.findComponent('from-' + this.batchs[index+1]))-1; //return stranges numbers
            if(index == 0) start = -1; //workaround
            this.generate_indicator(batch, start);
            start++;
            for(msg in events) {
                var date_badge = this.check_day(msg.origin_server_ts, last_day, index);
                if(date_badge != null) {
                    this.msgs_box._contents.addComponentAt(date_badge, start);
                    last_day = msg.origin_server_ts;
                    start++;

                    trace(date_badge.top);
                    trace(date_badge.screenTop);
                };

                var comp: Component;
                if(Reflect.field(msg.content, 'body') != null || Reflect.field(msg.content, 'msgtype') != null) {
                    comp = new Message(msg).component;
                } else {
                    comp = new Badge(msg).component;
                    if(Reflect.field(msg.content, 'creator') != null) {
                        this.min_msg_reached = true;
                    }
                }
                this.msgs_box._contents.addComponentAt(comp, start);
                start++;
            }

            var dtime = this.batchs_time_up[this.batchs[index+1]];
            var last_time = events.pop().origin_server_ts;
            var date_badge = this.check_day(last_time, dtime, index);
            if(date_badge != null) this.msgs_box._contents.addComponentAt(date_badge, start);
            this.batchs_time_up.remove(this.batchs[index+1]);
        }
    }

    @:access(haxe.ui.containers.ScrollView._contents) //Workaround for addComponentAt
    public function generate_indicator(batch:String, ?i:Int) {
        var comp: Component = new Component();
        comp.id = 'from-' + batch;
        if(i == null) this.msgs_box.addComponent(comp); else this.msgs_box._contents.addComponentAt(comp, i);
    }

    public function check_day(ts: Int, last_day: Null<Int>, batch_index: Int): Null<Component> {
        if(last_day == null) {
            if(this.batchs[batch_index-1] != null) {
                last_day = this.batchs_time_down[this.batchs[batch_index-1]];
                this.batchs_time_down.remove(this.batchs[batch_index-1]);
            }
            if(last_day == null) {
                last_day = ts;
                this.batchs_time_up[this.batchs[batch_index]] = ts;
                Main.log.log('info', 'Unknow last day of previus batch');
            }
        }
        var d1 = Date.fromTime(ts);
        var d2 = Date.fromTime(last_day);

        if(d1.getDay() != d2.getDay() || d1.getMonth() != d2.getMonth()) {
            return new Date_badge(d1).component;
        } else {
            return null;
        }
    }

    private function compare_batchs(s1: String, s2: String): Int {
        var a1 = s1.split('-');
        a1 = [a1[0]].concat(a1[1].split('_'));
        var a2 = s2.split('-');
        a2 = [a2[0]].concat(a2[1].split('_'));

        for(i in 1...a1.length-1) {
            if(a1[i] > a2[i]) {
                return 1;
            } else if(a1[i] < a2[i]) {
                return -1;
            }
        }
        Main.log.log('error', 'Duplicated batch recived');
        return 0;
    }

    override private function generate_component(): Component {
        var box = new VBox();
        this.info_bar = new Info();
        this.msgs_box = new ScrollView();

        this.msgs_box.percentWidth = 100;
        this.msgs_box.percentContentWidth = 100;
        this.msgs_box.percentHeight = 100;
        this.msgs_box.addClass('panel-body');
        this.msgs_box.addClass('scroll-fix');
        //this.msgs_box.id = 'msgs';
        //msgs_box.layout = new HorizontalLayout();

        box.percentWidth = 59;
        box.percentHeight = 100;
        box.addClass('panel');
        box.addComponent(this.info_bar.component);
        box.addComponent(msgs_box);
        return box;
    }
}