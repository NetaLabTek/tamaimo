# Tamaimoe
***Advertencia: esta aplicación se encuentra en un estado prealpha, por lo solo se encuentran aproximadamente solo el 20% terminada y hay que pulir muchas cosas***

![Tamaimo](assets/presentation/main-html5.jpg)

Tamaimoe es una aplicación de mensajería instantánea que usa el protocolo matrix(en un futuro otros, como **xmpp**), por lo tanto es federada(descentralizada) y unificada con otras plataformas, ademas es multiplataforma, disponiendo de una versión web *con un servidor web(pronto) para ofrecer archivos y enlaces de invitación*. 

Por otro lado brinda unas funcionalidades de chat bastante completas, ademas de brindar funcionalidades extras para poder crear comunidades y blogs, teniendo el modo blog y modo foro. También tiene un enfoque hacia la personalización, permitiendo temas, plugins, widgets, bots, integración con otras plataformas y personalización a las comunidades sin romper la estética uniforme de la app. Dándole una gran flexibilidad para varios tipos usos. También tiene una amplia configuración dispuesta para facilitar las tareas de administración y ser sencilla.

Por otra parte, brinda una interfaz que busca ergonomia, sin redundancia y minimalismo.

También es perfecta para reuniones, teniendo un chat de voz propio que no dependerá de la tecnología webrtc, pudiendo integrar codecs diseñados para la voz. Ademas de disponer de una interfaz de voz personalizable, adaptable e integrable(overlay en videojuegos).

Desde la parte técnica al estar hecha en haxe y haxeui, es adaptable a muchas plataformas y backends gráficos, la aplicación se puede usar con la interfaz personalizada, nativa del sistema o en consola. Ademas, se puede transpilar a varios lenguajes *js, c, c++, neko, java(?)* y a varios dispositivos como PC, moviles, consolas, ...

## Instalacion
Todavía no esta completa como para ser instalada.

## Compilacion 
El proceso de compilación puede ser un poco tedioso debido a que tanto esta app como la plataforma haxeUI se encuentran incompletas. 
Lo primero, sera tener instalado **haxe y haxelib**, recomendamos la versión *3.4*.

Lo segundo sera instalar las siguientes librerías de haxelib:
``` 
haxe-crypto
matrix-im
simplebinds
easylog
mime
compiletime
hext-sort
hext-search
markdown
``` 
Ademas tendremos que instalar por git, por el momento, los forks de [akifox-http](https://github.com/endes123321/akifox-asynchttp) y [haxeui-core](https://github.com/endes123321/haxeui-core).

Por ultimo tendremos que compilar con haxe, podemos instalar openfl y usar ```openfl run``` o compilar la versión html con ```haxe build.hxml``` o la versión nativa con ```haxe build-wxwidgets.hxml``` o la versión de consola con ```haxe build-curse.hxml```.

## Desarrollar
Todavía no esta completa como para desarrollar plugins, temas, etc ...

### Temas

### Plugins

### bots, integraciones, ...
Esto se hace a través del protocolo matrix, en [su pagina puedes encontrar más información.](https://matrix.org/docs/projects/try-matrix-now.html#client-sdks)

## Contribuir
Tengo que organizar muchas cosas para este punto, simplemente abre un issue o haz un pull request y hablamos.

## Creditos
TODO.