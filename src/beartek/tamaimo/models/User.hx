package beartek.tamaimo.models;

import beartek.matrix_im.client.types.enums.Presences;
import beartek.matrix_im.client.types.Event;
import beartek.matrix_im.client.types.User as User_t;

@:build(simple.bind.Macros.build())
class User {
    public static var users_list: Map<String, {rooms: Int, user: User}> = new Map();
    static var onnew_triggers: Array<{rooms: Int, user: User}->Void> = [];
    static var onremove_triggers: Array<User->Void> = [];
    static var syncer = false;

    public static function register_onnew(trigger: {rooms: Int, user: User}->Void) {
        onnew_triggers.push(trigger);
    }

    public static function register_onremove(trigger: User->Void) {
        onremove_triggers.push(trigger);
    }

    private static function onadd(u:User_t) {
        if(users_list[u.toString()] != null) {
            users_list[u.toString()].rooms++;
            if(users_list[u.toString()].rooms > 3) users_list[u.toString()].user.start_sync_presence();
        } else {
            users_list[u.toString()] = {rooms: 1, user: new User(new User_t(u.toString()))};
        }

        for(t in onnew_triggers) {
            t(users_list[u.toString()]);
        }
    }

    private static function onremove(u:User_t) {
        if(users_list[u.toString()] != null) {
            users_list[u.toString()].rooms--;
            if(users_list[u.toString()].rooms < 3) users_list[u.toString()].user.stop_sync_presence();

            if(users_list[u.toString()].rooms < 1) {
                users_list[u.toString()] = null;

                for(t in onremove_triggers) {
                    t(users_list[u.toString()].user);
                }
            }  
        } else {
            Main.log.log('error', 'User ' + u + 'Already removed from user list');
        }

    }


    public var id(default,null): beartek.matrix_im.client.types.User;

    @:bind public var display_name(default,null): String; //TODO: make readonly and map by room
    @:bind public var avatar_url(default,null): String; //TODO: make readonly and map by room

    //public function set_display_name(name: String): String {
    //    Main.conn.profile.set_displayname(name ,this.id, function() {
    //    });
    //    return name;
    //}

    //public function set_avatar_url(url: String): String {
    //    Main.conn.profile.set_avatar(url, this.id, function() {
    //    });
    //    return url;
    //}

    @:bind var presence(default,default): Presences; //TODO: make readonly
    @:bind public var active(default,default): Bool; //TODO: make readonly
    @:bind public var last_active_ago(default,default): Int; //TODO: make readonly
    //var last_read(): 
    @:bind public var status(get,set): String; //TODO: make readonly
    var _last_status: String;

    //public function set_presence(status: Presences): Presences {
    //    Main.conn.presence.set_presence(this.id, this.presence);
    //    return status;
    //}

    //public function set_status(status: String): String {
    //    Main.conn.presence.set_presence(this.id, this.presence, status);
    //    return status;
    //}

    public function get_status(): String {
        //Main.conn.presence.get_status, get statuses
        Main.conn.presence.get_presence(this.id, function(user_status: beartek.matrix_im.client.types.replys.User_status) {
            this.status = user_status.status_msg;
            this.presence = user_status.presence;
            this.active = user_status.currently_active;
            this.last_active_ago = user_status.last_active_ago;
        });
        return _last_status;
    }

    public function set_status(s:String): String {
        this._last_status = s;
        return s;
    }


    public function new(id: beartek.matrix_im.client.types.User) {
        this.id = id;
    }

    public static function register_syncer() {
        Main.conn.sync.add_presence_handler(function(events: Array<Event<m.Presence>>) {
            for(e in events) {
                var id = e.content.user_id;
                if(users_list.exists(id)) {
                    users_list[id].user.display_name = e.content.displayname;
                    users_list[id].user.avatar_url = e.content.avatar_url;

                    users_list[id].user.presence = e.content.presence;
                    users_list[id].user.last_active_ago = e.content.last_active_ago;
                    users_list[id].user.active = e.content.curently_active;
                } else {
                    users_list[id] = {user: new User(new User_t(id)), rooms: 0};
                    users_list[id].user.display_name = e.content.displayname;
                    users_list[id].user.avatar_url = e.content.avatar_url;

                    users_list[id].user.presence = e.content.presence;
                    users_list[id].user.last_active_ago = e.content.last_active_ago;
                    users_list[id].user.active = e.content.curently_active;
                }
            }
        });
    }

    public function start_sync_presence() {
        if(!Main.conn.presence.list.exists(this.id)) {
            Main.conn.presence.list[this.id] = null;
        }
    }

    public function stop_sync_presence() {
        if(Main.conn.presence.list.exists(this.id)) {
            Main.conn.presence.list[this.id] = null;
        }
    }
}