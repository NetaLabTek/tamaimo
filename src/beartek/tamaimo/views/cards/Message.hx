package beartek.tamaimo.views.cards;

import haxe.ui.macros.ComponentMacros;
import haxe.ui.core.Component;
import haxe.ui.components.Image;
import haxe.ui.components.Label;
import haxe.ui.containers.Box;
import beartek.matrix_im.client.types.Event;
import beartek.matrix_im.client.types.Content_uri;
import beartek.matrix_im.client.types.enums.Msg_types;
import beartek.tamaimo.models.User as User_model;


class Message extends View {
    var msg: Event<m.room.Message>;

    public function new(msg: Event<m.room.Message>) {
        super();

        if(Reflect.field(msg.content, 'body') == null && Reflect.field(msg.content, 'msgtype') == null) {
            throw 'Event isnt a Message';
        };
        this.msg = msg;

        var userimg: Image = this.component.findComponent('userimg');
        var username: Label = this.component.findComponent('username');
        var time: Label = this.component.findComponent('time');
        var content: Box = this.component.findComponent('content');

        var u = User_model.users_list[msg.sender];
        if(u == null) {
            Main.log.log('error', 'User Model doest exist');
            return;
        }
        try {
            //TODO: Storage
            if(u.user.avatar_url != null) {
                //URLs only works on js
                #if js
                userimg.resource = new Content_uri(u.user.avatar_url).toURL(Main.conn.server_url);
                #end
            }
        } catch(e: String) {
            Main.log.log('error', 'Error getting user img: ' + e);
        }
        username.text = User_model.users_list[msg.sender].user.display_name;
        time.text =  DateTools.format(Date.fromTime(msg.origin_server_ts), '%H:%M');
        this.generate_content(content);
    }

    public function generate_content(c: Box) {
        switch this.msg.content.msgtype {
            case Text:
                //TODO: highlight URLs, process html and markdown
                var text = new Label();
                var link_box = this.generate_link_box(this.msg.content.body, this.msg.origin_server_ts);
                //text.percentHeight = if(link_box != null) 60 else 100;
                text.percentWidth = 100;
                text.text = this.msg.content.body;
                c.addComponent(text);
                if(link_box != null) c.addComponent(link_box);
            case Emote:
                var badge = new Badge(this.msg);
                this.component = badge.component;
            case Notice:
                var text = new Label();
                //text.percentHeight = 100;
                text.percentWidth = 100;
                text.text = this.msg.content.body;
                c.borderSize = 3;
                c.addComponent(text);
            case Image:
                var text = new Label();
                text.text = this.msg.content.body;

                c.addComponent(text);
                try {
                    c.addComponent(this.generate_file_box(this.msg.content.filename, this.msg.content.info.w + 'x' + this.msg.content.info.h + ' ' + this.msg.content.info.size, this.msg.content.info.thumbnail_url, this.msg.content.msgtype, this.msg.content.url));
                } catch(e:Dynamic) {
                    Main.log.log('error', 'error generating file box: ' + Std.string(e));
                }
            case File:
                var text = new Label();
                text.text = this.msg.content.body;

                c.addComponent(text);
                try {
                    c.addComponent(this.generate_file_box(this.msg.content.filename, this.msg.content.info.mimetype + ' ' + this.msg.content.info.size, this.msg.content.info.thumbnail_url, this.msg.content.msgtype, this.msg.content.url));
                } catch(e:Dynamic) {
                    Main.log.log('error', 'error generating file box: ' + Std.string(e));
                }
            case Location:
                var icon = new Image();
                var text = new Label();

                icon.percentHeight = 100;
                icon.percentWidth = 10;
                icon.horizontalAlign = 'left';
                text.percentHeight = 100;
                text.percentWidth = 90;
                text.text = this.msg.content.body;

                c.addComponent(icon);
                c.addComponent(text);
                c.onClick = function(e: Dynamic) {
                var r = ~/geo:(.+),(.+)/g;
                if(r.match(this.msg.content.geo_uri)) {
                    #if js
                        js.Browser.window.open('https://www.openstreetmap.org/#map=19/' + r.matched(1) + '/' + r.matched(2));
                    #end
                }
                };
            case Video:
                var text = new Label();
                text.text = this.msg.content.body;

                c.addComponent(text);
                try {
                    c.addComponent(this.generate_file_box(this.msg.content.filename, this.msg.content.info.w + 'p ' + this.msg.content.info.duration + ' ' + this.msg.content.info.size, this.msg.content.info.thumbnail_url, this.msg.content.msgtype, this.msg.content.url));
                } catch(e:Dynamic) {
                    Main.log.log('error', 'error generating file box: ' + Std.string(e));
                }
            case Audio:
                var text = new Label();
                text.text = this.msg.content.body;

                c.addComponent(text);
                try {
                    c.addComponent(this.generate_file_box(this.msg.content.filename, this.msg.content.info.duration + ' ' + this.msg.content.info.size, this.msg.content.info.thumbnail_url, this.msg.content.msgtype, this.msg.content.url));
                } catch(e:Dynamic) {
                    Main.log.log('error', 'error generating file box: ' + Std.string(e));
                }
            case _:
                Main.log.log('error', 'Not implemented');         
        }
    }

    private function generate_file_box(name: String, info: String, thumbnail: String, type: Msg_types, url: String): Component {
        var comp = ComponentMacros.buildComponent('UI/cards/file_box.xml');
        var icon: Image = comp.findComponent('icon');
        var filename: Label = comp.findComponent('filename');
        var info_l: Label =comp.findComponent('info');

        //URLs only works on js
        #if js
        icon.resource = if(thumbnail != null) new Content_uri(thumbnail).toURL(Main.conn.server_url) else '';
        #end
        filename.text = if(name != null) name else '';
        info_l.text = if(info != null) info else '';
        comp.onClick = this.open_file(type, url);
        return comp;
    }

    private function generate_link_box(text: String, ts:Int): Null<Component> {
        var s = ~/((\w+:\/\/)[-a-zA-Z0-9:@;?&=\/%\+\.\*!'\(\),\$_\{\}\^~\[\]`#|]+)/g;
        if(!s.match(text)) return null;

        var comp = ComponentMacros.buildComponent('UI/cards/link_box.xml');

        Main.conn.content.preview_url(s.matched(1), ts, function(data: Map<String, String>) {
            var icon: Image = comp.findComponent('icon');
            var title: Label = comp.findComponent('title');
            var description: Label = comp.findComponent('description');

            //TODO: No image image
            try {
                //URLs only works on js
                #if js
                icon.resource = if(data['og:image'] != null || data['og:image'] != '') new Content_uri(data['og:image']).toURL(Main.conn.server_url) else '';
                #end
            } catch(e: String) {
                Main.log.log('error', 'Error getting link img: ' + e);
            }
            title.text = if(data['og:title'] != null) data['og:title'] else '';
            description.text = if(data['og:description'] != null) data['og:description'].substr(0, 256) else '';
            comp.onClick = this.open_file(Text, s.matched(1));
        });
        
        return comp;
    }

    private function open_file(type: Msg_types, uri: String): Dynamic -> Void {
        var url = if(type != Text) new Content_uri(uri).toURL(Main.conn.server_url) else uri;
        return function(e: Dynamic) {
            #if js
                js.Browser.window.open(url);
            #end
        };
    }

    override private function generate_component(): Component {
        var card = ComponentMacros.buildComponent('UI/cards/msg.xml');
        return card;
    }
}