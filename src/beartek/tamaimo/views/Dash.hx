package beartek.tamaimo.views;

import haxe.ui.core.Component;
import haxe.ui.containers.HBox;
import beartek.tamaimo.views.bars.Menu;
import beartek.tamaimo.views.bars.Chat;
import beartek.tamaimo.views.bars.Mm;
import beartek.tamaimo.models.Room as Room_model;

class Dash extends View {
    public function new() {
        super();

        var chat = new Chat();
        var mm = new Mm();
        var menu = new Menu(function(id: String) {
            chat.selected_room = Room_model.joined_rooms[id];
            mm.selected_room = Room_model.joined_rooms[id];
        });

        this.component.addComponent(menu.component);
        this.component.addComponent(chat.component);
        this.component.addComponent(mm.component);
    }

    override private function generate_component(): Component {
        var box = new HBox();
        box.percentHeight = 100;
        box.percentWidth = 100;
        return box;
    }
}