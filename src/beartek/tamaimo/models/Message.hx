package beartek.tamaimo.models;

import beartek.matrix_im.client.types.Room;
import beartek.matrix_im.client.types.Event;

abstract Message(Event<m.room.Message>) {

    inline public function new(m:Event<m.room.Message>) {
        this = m;
    }

    inline public function get_id() {

    }

    inline public function get_timestamp() {
        
    }

    inline public function get_sender() {
        
    }

    inline public function get_sender_name() {
        
    }

    inline public function get_room() {
        
    }


    inline public function get_msgtype() {
        
    }

    inline public function get_body() {
        
    }


    inline public function get_info() {
        
    }

    inline public function get_url() {
        
    }

    inline public function get_geo() {
        
    }

}